import numpy as np
import pytesseract
from PIL import Image
import cv2 as cv

class CardDetector:
    def __init__(self, scale=4, card_w=223, card_h=310):
        self.scale = scale
        self.scale2 = self.scale ** 2
        self.card_w = card_w
        self.card_h = card_h
        self.card_area = self.card_w * self.card_h / 4

    def detect(self, image):
        scaled = cv.resize(image, (0, 0), fx=(1.0 / self.scale), fy=(1.0 / self.scale), interpolation=cv.INTER_AREA)
        blur = cv.bilateralFilter(scaled, 5, 17, 17)
        gray = cv.cvtColor(blur, cv.COLOR_BGR2GRAY)
        edges = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV, 15, 3)
        # edges = cv.Canny(edges, 96, 128)

        (_, c, _) = cv.findContours(edges.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        contours = sorted(c, key=lambda contour: -cv.contourArea(contour))

        cards = []
        for contour in contours:
            epsilon = 0.1 * cv.arcLength(contour, True)
            approx = cv.approxPolyDP(contour, epsilon, True)
            if (len(approx) == 4 and cv.contourArea(approx) > self.card_area / self.scale2):
                cards.append(approx * self.scale)

        return cards

    def extract(self, image, approx_bounds):
        cards = []
        for approx in approx_bounds:
            if(approx is None or len(approx) != 4):
                return np.zeros([self.card_h, self.card_w], np.int8)

            p1, p2, p3, p4 = approx
            d1 = max(self._dist(p1, p2), self._dist(p3, p4))
            d2 = max(self._dist(p2, p3), self._dist(p4, p1))

            # h = int(round(max(d1, d2)))
            # w = int(round(self.card_w/self.card_h*h))
            w, h = (self.card_w, self.card_h)

            if (d1 > d2):
                pts2 = np.float32([[w, h], [w, 0], [0, 0], [0, h]])
            else:
                pts2 = np.float32([[w, 0], [0, 0], [0, h], [w, h]])

            pts1 = np.float32(approx)
            transform = cv.getPerspectiveTransform(pts1, pts2)
            card = cv.warpPerspective(image, transform, (w, h))

            cards.append(card)

        return cards

    def processCards(self, cards, denoise=False, bilateral_filter=False, equilise_channels=False):
        processed = []
        for card in cards:
            if denoise:
                card = cv.fastNlMeansDenoisingColored(card)
            if bilateral_filter:
                card = cv.bilateralFilter(card, 11, 17, 17)
            if equilise_channels:
                card[:,:,0] = cv.equalizeHist(card[:,:,0])
                card[:,:,1] = cv.equalizeHist(card[:,:,1])
                card[:,:,2] = cv.equalizeHist(card[:,:,2])
            processed.append(card)
        return processed

    def read(self, cards):
        for card in cards:
            blur = cv.bilateralFilter(card, 11, 31, 5)
            gray = cv.cvtColor(blur, cv.COLOR_BGR2GRAY)
            thresh = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 3)

            boxes = pytesseract.image_to_string(Image.fromarray(thresh), boxes=True)
            boxes = boxes.split("\n")
            for box in boxes:
                args = box.split(" ")
                if (len(args) == 6):
                    c, x1, y1, x2, y2, _ = args
                    x1 = int(x1)
                    x2 = int(x2)
                    y1 = self.card_h - int(y1)
                    y2 = self.card_h - int(y2)
                    cv.line(card, (x1, y1), (x1, y2), (0, 0, 255))
                    cv.line(card, (x2, y1), (x2, y2), (0, 0, 255))
                    cv.line(card, (x1, y1), (x2, y1), (0, 0, 255))
                    cv.line(card, (x1, y2), (x2, y2), (0, 0, 255))

    def show(self, name, img, scale=1):
        image = img
        if scale != 1:
            image = cv.resize(img, (0,0), fx=scale, fy=scale)
        cv.imshow(name, image)

    def showCards(self, cards):
        for i in range(len(cards)):
            cv.imshow(str(i), cards[i])

    def drawContours(self, image, contours, color=(0,255,0), thickness=1):
        for contour in contours:
            cv.drawContours(image, [contour], -1, color, thickness)

    def _dist(self, p1, p2):
        x1, y1 = p1[0]
        x2, y2 = p2[0]
        dx = x1 - x2
        dy = y1 - y2
        return (dx * dx + dy * dy) ** (0.5)