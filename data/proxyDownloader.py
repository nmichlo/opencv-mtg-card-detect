from mtg.data import proxyScrape
import multiprocessing as mp
from urllib import request
import io
import zipfile
import urllib

ATTEMPTS = 10
PROXIES = proxyScrape.ProxyScrape()

def downloadThreadedProxies(url_file_tuples, threads=16):
    m = mp.Pool(processes=threads)
    m.starmap(_download, url_file_tuples)

def directDownloadUnzip(url, folder, fileName):
    if folder.endswith("/"):
        folder = folder[:-1]
    file = "{}/{}".format(folder, fileName)

    urllib.request.urlretrieve(url, "{}.zip".format(file))

    zip_ref = zipfile.ZipFile("{}.zip".format(file), 'r')
    zip_ref.extractall(folder)
    zip_ref.close()


def _download(url, file):
    for i in range(ATTEMPTS):
        try:
            proxy = PROXIES.randomProxy()

            proxy_handler = request.ProxyHandler(proxy)
            proxy_auth_handler = request.ProxyBasicAuthHandler()
            opener = request.build_opener(proxy_handler, proxy_auth_handler)
            open_url = opener.open(url)  # add timeout?
            read = open_url.read()

            f = io.FileIO(file, "w")
            f.write(read)

            string = "Downloaded (try={}): {} : {}".format(i, url, file)
            print(string)

            return
        except:
            continue

    string = "Failed (tries={}): {} : {}".format(ATTEMPTS, url, file)
    print(string)