import json
import fuzzyset
import os
from mtg.data.proxyDownloader import directDownloadUnzip

class MtgCards:

    FOLDER = "json"
    FILE_CARDS = "AllCards.json"
    FILE_CARDS_X = "AllCards-x.json"

    FILE_CARDS_URL   = "https://mtgjson.com/json/AllCards.json.zip"
    FILE_CARDS_X_URL = "https://mtgjson.com/json/AllCards-x.json.zip"

    KEYS_CARD = [
        "cmc", "colors", "colorIdentity", "imageName", "layout", "manaCost",
        "name", "power", "text", "toughness", "type", "types"
    ]

    def __init__(self, x=False):
        fileName = MtgCards.FILE_CARDS_X if x else MtgCards.FILE_CARDS
        file = "{}/{}".format(MtgCards.FOLDER, fileName)

        if not os.path.exists(MtgCards.FOLDER):
            print("\tCard JSON Dir does not exist: {} Making dir!".format(MtgCards.FOLDER))
            os.makedirs(MtgCards.FOLDER)

        if not os.path.isfile(file):
            url = MtgCards.FILE_CARDS_X_URL if x else MtgCards.FILE_CARDS_URL
            print("\tCard JSON Data does not exist at: {} DOWNLOADING from: {}".format(file, url))
            directDownloadUnzip(url, MtgCards.FOLDER, fileName)

        print("Loading card data from: " + file)

        self.data = json.loads(open(file).read())
        self.card_names = [self.data[card]["name"] for card in self.data]
        self.fuzzy_card_names = fuzzyset.FuzzySet(self.card_names)

        print("Finished loading card data.")

    def fuzzyNames(self, fuzzy_names):
        names = []
        for fuzzy_name in fuzzy_names:
            try:
                options = self.fuzzy_card_names.get(fuzzy_name)
                names.append(options[0][1])
            except:
                continue
        return names

    def getCard(self, name):
        return self.data[name] if name in self.data else None


class MtgSets:

    FOLDER = "json/"
    FILE_SETS = "AllSets.json"
    FILE_SETS_X = "AllSets-x.json"

    FILE_SETS_URL   = "https://mtgjson.com/json/AllSets.json.zip"
    FILE_SETS_X_URL = "https://mtgjson.com/json/AllSets-x.json.zip"

    KEYS_SET = [ "booster", "border", "cards", "code", "magicCardsInfoCode", "name", "onlineOnly", "releaseData", "type" ]
    KEYS_CARD = [
        "artist", "cmc", "colors", "colorIdentity", "id", "imageName", "layout", "manaCost", "mciNumber",
        "multiverseid", "name", "number", "rarity", "text", "type", "types"
    ]

    KEY_BOOSTER = "booster"
    KEY_BORDER = ""

    def __init__(self, x=False):
        fileName = MtgSets.FILE_SETS_X if x else MtgSets.FILE_SETS
        file = "{}/{}".format(MtgCards.FOLDER, fileName)

        print("Loading set data from: " + file)

        if not os.path.exists(MtgSets.FOLDER):
            print("\tCard JSON Set does not exist: {} Making dir!".format(MtgSets.FOLDER))
            os.makedirs(MtgSets.FOLDER)

        if not os.path.isfile(file):
            url = MtgSets.FILE_SETS_X_URL if x else MtgSets.FILE_SETS_URL
            print("\tCard JSON Set does not exist at: {} DOWNLOADING from: {}".format(file, url))
            directDownloadUnzip(url, MtgCards.FOLDER, fileName)

        self.data = json.load(open(file))
        self.codes = [key for key in self.data if not "onlineOnly" in self.data[key]]
        self.fuzzy_names = fuzzyset.FuzzySet([self.data[key]["name"] for key in self.data], gram_size_upper=20)
        self.name2code = {self.data[key]["name"]: self.data[key]["code"] for key in self.data}

        self.mvid2set = {}
        self.mvid2card = {}

        self.cardname2mvids = {}

        for key in self.codes:
            for card in self.data[key]['cards']:
                if not "multiverseid" in card:
                    continue

                mvid = card["multiverseid"]
                name = card["name"]
                self.mvid2card[mvid] = name
                self.mvid2set[mvid] = key

                if not name in self.cardname2mvids:
                    self.cardname2mvids[name] = []
                self.cardname2mvids[name].append(mvid)

        print("Finished loading set data.")

    def all_sets_mvids(self, set_codes=None):
        sets = self.codes if None else set_codes
        mvids = {}
        for key in sets:
            mvids[key] = [card["multiverseid"] for card in self.data[key]['cards'] if "multiverseid" in card]
        return mvids

    def all_mvids_sets(self, set_codes):
        mvids = self.all_sets_mvids(set_codes)
        return {item: key for key in mvids for item in mvids[key]}

    def mvidToCard(self, mvid):
        return self.data[self.mvid2set[mvid]]['cards'][self.mvid2card[mvid]]

    def mvidToCardName(self, mvid):
        return self.mvid2card[mvid] if mvid in self.mvid2card else ""

    def mvidToSet(self, mvid):
        return  self.mvid2set[mvid] if mvid in self.mvid2set else ""

    def cardNameToMvids(self, name):
        return self.cardname2mvids[name] if name in self.cardname2mvids else []

    def cardNameToMvid(self, name):
        return self.cardname2mvids[name][0] if name in self.cardname2mvids else None

    def fuzzyToSetNames(self, fuzzy_names):
        names = []
        for fuzzy_name in fuzzy_names:
            try:
                options = self.fuzzy_names.get(fuzzy_name)
                names.append(options[0][1])
            except:
                continue
        return names

    def fuzzySetNamesToSetCodes(self, fuzzy_names):
        return self.setNamesToSetCodes(self.fuzzyToSetNames(fuzzy_names))

    def setNamesToSetCodes(self, names):
        return [self.name2code[name] if name in self.name2code else "" for name in names]

    def allSetCodes(self):
        return self.codes