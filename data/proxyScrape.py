from urllib import request
import bs4
import os
from os import path
import json
import random
import time


class ProxyScrape:

    def __init__(self, folder="json/", fileName="proxies.json", force_scrape=False, days=10):
        self.proxies = []

        time_ms = int(round(time.time() * 1000))

        if folder.endswith("/"):
            folder = folder[:-1]
        file = "{}/{}".format(folder, fileName)

        if not os.path.exists(folder):
            print("\tProxies save dir does not exist: {} Making dir!".format(folder))
            os.makedirs(folder)

        if not path.isfile(file) or force_scrape:
            if force_scrape:
                print("Forced proxy scrape... Scraping proxies!")
            else:
                print("Proxy file: {} not found... Scraping proxies!".format(file))
            self.proxies = self._scrapeAndDump(file, time_ms)
        else:
            try:
                read_obj = json.load(open(file, "r"))
                proxies = read_obj["proxies"]
                ms_time_created = read_obj["created"]
                if time_ms - ms_time_created > 1000 * 60 * 60 * 24 * days:
                    print("Proxy file is old... Scraping proxies!")
                    self.proxies = self._scrapeAndDump(file, time_ms)
                else:
                    self.proxies = proxies
            except:
                print("Invalid proxy file... Scraping proxies!")
                self.proxies = self._scrapeAndDump(file, time_ms)
                return

    def _scrapeAndDump(self, file, time_ms):
        proxies = self._scrapeHideMyAssProxies()
        proxies_json = json.dumps({"created": time_ms, "proxies": proxies}, indent=2)
        proxies_file = open(file, "w")
        proxies_file.write(proxies_json)
        print("Saved: {} proxies to: {}".format(len(proxies), file))
        return proxies

    def randomProxy(self):
        index = random.randint(0, len(self.proxies) - 1)
        return self.proxies[index]

    def _scrapeHideMyAssProxies(self, hma_url="http://proxylist.hidemyass.com/search-1756621"):
        html = request.urlopen(hma_url).read()
        print("Scraping: \"{}\" for proxies!".format(hma_url))
        # html = open("src.html").read()
        soup = bs4.BeautifulSoup(html, "lxml")
        tbody = soup.find_all("tbody")[0]
        trows = tbody.find_all("tr")

        proxies = []
        for row in trows:
            entries = row.find_all("td")
            entry_ip = entries[1].contents[1]
            port = str(entries[2].string).strip()
            protocal = str(entries[6].string).strip().lower()

            style = str(entry_ip.contents[1].string)[2:]
            entry_data = entry_ip.contents[1:]
            style_dict = {dat[0]: "display:inline" in dat[1] for dat in [s.split("{") for s in style.split(".")]}
            false_list = [klass for klass in style_dict if style_dict[klass] == False]

            ip = ""
            for tag in entry_data:
                if ("bs4.element.NavigableString" in str(tag.__class__)):
                    ip += str(tag).strip()
                    continue
                elif ("bs4.element.Tag" in str(tag.__class__)):
                    if (tag.string is None):
                        continue
                    if not "display:none" in str(tag):
                        valid = True
                        for key in false_list:
                            if (key in str(tag)):
                                valid = False
                                break
                        if valid:
                            ip += str(tag.string).strip()

            proxy = "{}://{}:{}".format(protocal, ip, port)
            proxies.append({protocal: proxy})

        print("Scraped Proxies: {}".format(proxies))
        return proxies
