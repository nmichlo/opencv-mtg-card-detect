import cv2 as cv
import json
import os
import random
import numpy
from pyflann import FLANN

class AnnFeatures:

    def __init__(self, mtg_images, folder="data/ann", name=None, bins=128, nfeatures=200, nrandom=1, normalise=False, remove_flat=True, features_thresh=0.2, blur=False, seed=777, force_features=False, force_means=False, force_hists=False):

        if folder.endswith("/"):
            folder = folder[:-1]

        print("INITIALISING: Features")
        if not os.path.exists(folder):
            print("\tFeatures dir {} does not exist... Making!".format(folder))
            os.mkdir(folder)

        self.mtg_images = mtg_images
        self.name = name
        self.bins = bins
        self.nfeatures = nfeatures
        self.nrandom = nrandom
        self.normalise = normalise
        self.features_thresh = features_thresh
        self.remove_flat = True
        self.blur = True
        self.blur_amount = 5
        self.seed = seed

        name = "" if name is None else "_" + name
        self.features_file = "{}/features_random{}.json".format(folder, name)
        self.means_file = "{}/features_kmeans{}.json".format(folder, name)
        self.hists_file = "{}/feature_hists{}.json".format(folder, name)

        if not os.path.isfile(self.features_file) or force_features:
            self._genFeatures()

        if not os.path.isfile(self.means_file) or force_means or force_features:
            self._genMeans()

        self.kmeans = numpy.array(json.load(open(self.means_file, 'r')), numpy.float32)
        self.flann_features = FLANN()
        self.flann_features.build_index(self.kmeans)
        # self.asdf = FLANN_INDEX(self.kmeans)
        # self.flann_features = cv.flann.Index(self.kmeans, {'algorithm':1}, distType=2)

        if not os.path.isfile(self.hists_file) or force_hists or force_means or force_features:
            self._genHists()

        mvid_hist = json.load(open(self.hists_file, 'r'))
        self.hists = numpy.array([mvid_hist[mvid] for mvid in mvid_hist], numpy.int32)
        self.index_mvid = {index: int(mvid) for index, mvid in enumerate(mvid_hist)}
        self.mvid_index = {int(mvid): index for index, mvid in enumerate(mvid_hist)}
        # self.flann_hists = cv.flann.Index(self.hists, {'algorithm':1}, distType=2)
        self.flann_features = FLANN()
        self.flann_features.build_index(self.hists)

        print("DONE: Features")

    def imageFeatures(self, images):
        all_features = []
        for image in images:
            gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

            # sift = cv.SIFT_create()
            # kp, features = sift.detectAndCompute(gray, None)

            orb = cv.ORB_create(nfeatures=self.nfeatures)
            kp, features = orb.detectAndCompute(gray, None)

            if self.normalise:
                for i, feature in enumerate(features):
                    max = numpy.max(feature)
                    features[i] = feature / (max if max != 0 else 1)
                    for j, val in enumerate(features[i]):
                        features[i:j] = min(self.features_thresh, val)

                if self.remove_flat:
                    features = [feature for feature in features if not numpy.sum(feature) >= len(feature)*self.features_thresh]

            features = features[:self.nfeatures]

            all_features.append(features)
        return numpy.array(all_features, numpy.float32)

    def nearestFeatureIndices(self, all_features):
        all_nearest_features = []
        for features in all_features:
            nearest_features_indices, distances = self.flann_features.nn_index(features, num_neighbors=1)
            all_nearest_features.append(nearest_features_indices)
        return all_nearest_features

    def cardHists(self, cards):
        all_features = self.imageFeatures(cards)
        all_nearest_indices = self.nearestFeatureIndices(all_features)
        all_hists = []

        for nearest_index in all_nearest_indices:
            hist, edges = numpy.histogram(nearest_index, bins=self.bins)
            hist = numpy.array(hist, numpy.int32)
            all_hists.append(hist)

        return all_hists

    def nearestHists(self, all_hists, num=5):
        all_nearest_hists = []
        for hist in all_hists:
            nearest_hist, distances = self.flann_hists.nn_index(hist, num_neighbors=num)
            append = nearest_hist[0].tolist()
            all_nearest_hists.append(append)
        return all_nearest_hists

    def nearestMvids(self, cards, nnearest=5):
        all_card_hists = self.cardHists(cards)
        all_nearest_indices = self.nearestHists(all_card_hists, num=nnearest)
        all_nearest_mvids = []
        for nearest in all_nearest_indices:
            all_nearest_mvids.append([self.index_mvid[index] for index in nearest])
        return all_nearest_mvids

    def _genFeatures(self):
        random.seed(self.seed)
        print("\tGenerating random features set {}... from {} random of each {} image features".format(self.features_file, self.nrandom, self.nfeatures))
        with open(self.features_file, 'w') as file:
            for i, mvid in enumerate(self.mtg_images.mvids_set):
                img = self.mtg_images.getImage(mvid)
                if self.blur:
                    img = cv.blur(img, (self.blur_amount, self.blur_amount))
                features = self.imageFeatures([img])[0].tolist()

                for j in range(min(self.nrandom, len(features))):
                    r = random.randint(0, len(features)-1)
                    ran = features[r]
                    features.remove(ran)
                    file.write(json.dumps(ran) + "\n")

                if i % 1000 == 0:
                    print("\t\t({} of {})".format(i, len(self.mtg_images.mvids_set)))

    def _genMeans(self):
        print( "\tGenerating kmeans features set {}... from random features set {}!".format(self.means_file, self.features_file) )
        descriptors = []
        with open(self.features_file, "r") as file:
            for i, line in enumerate(file):
                try:
                    feature = numpy.array(json.loads(line))
                    descriptors.append(feature)
                except:
                    print("\t\tFailed to read line ({})".format(i))
        print( "\t\t{} random features loaded... Performing kmeans!".format(len(descriptors)) )

        data = numpy.array(descriptors, numpy.float32)
        print(data.shape)
        criteria = (cv.TERM_CRITERIA_MAX_ITER + cv.TERM_CRITERIA_EPS, 1000, 0.0000001)
        compactness, labels, centers = cv.kmeans(data, self.bins, None, criteria, 100, cv.KMEANS_PP_CENTERS)
        print(compactness)
        means = centers.tolist()

        print("\t\tSaving means!")
        with open(self.means_file, 'w') as file:
            json.dump(means, file)

    def _genHists(self):
        print("\tGenerating feature histograms set {} from kmeans {}".format(self.hists_file, self.means_file))
        with open(self.hists_file, 'w') as file:
            file.write("{")
            for i, mvid in enumerate(self.mtg_images.mvids):
                img = self.mtg_images.getImage(mvid)
                if self.blur:
                    img = cv.blur(img, (self.blur_amount, self.blur_amount))
                fhist = self.cardHists([img])[0].tolist()
                file.write("\n\"{}\": {}{}".format(mvid, json.dumps(fhist), "," if i < len(self.mtg_images.mvids_set)-1 else ""))
                if i % 1000 == 0:
                    print("\t\t({} of {})".format(i, len(self.mtg_images.mvids_set)))
            file.write("\n}")

        print("\t\tDone generating and saving feature histograms!")



# features = AnnFeatures(MtgImages(folder="/Users/Nathan/Home/Local Workspace/mtg/cards"))