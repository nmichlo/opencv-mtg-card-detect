from mtg.data.mtgData import MtgSets
from mtg.data import proxyDownloader
import os
import cv2 as cv

class MtgImages:

    URL = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid={}&type=card"
    WIDTH = 223
    HEIGHT = 310

    def __init__(self, fuzzy_set_names=None, folder="images"):
        self.mtg_sets = None
        self.set_codes = None
        self.sets_mvids = None
        self.mvids = None
        self.mvids_set = None
        self.img_folders = None
        self.mvid_img_paths = None
        self.paths = None

        print("Initialising Images from: " + folder)

        if folder.endswith("/"):
            folder = folder[:-1]

        self.mtg_sets = MtgSets()
        self.set_codes = None

        if fuzzy_set_names is not None:
            set_names = self.mtg_sets.fuzzyToSetNames(fuzzy_set_names)
            self.set_codes = self.mtg_sets.setNamesToSetCodes(set_names)

            print("\tFuzzy Search: {}".format(fuzzy_set_names))
            print("\tFuzzy Results: {}".format(set_names))
        else:
            self.set_codes = self.mtg_sets.allSetCodes()

        self.sets_mvids = self.mtg_sets.all_sets_mvids(self.set_codes)
        self.mvids_set = self.mtg_sets.all_mvids_sets(self.set_codes)
        self.mvids = [mvid for mvid, set in self.mvids_set.items()]

        #make root dir
        if not os.path.exists(folder):
            print("\tRoot dir: {} does not exist... making it!".format(folder))
            os.makedirs(folder)

        #make dirs
        self.img_folders = ["{}/{}".format(folder, code) for code in self.set_codes]
        img_folders_not_exist = [img_folder for img_folder in self.img_folders if not os.path.exists(img_folder)]
        if len(img_folders_not_exist) > 0:
            print("\t{} folders do not exist... making them!".format(len(img_folders_not_exist)))
            for img_folder in img_folders_not_exist:
                os.mkdir(img_folder)

        #check existing images
        self.mvid_img_paths = { mvid: "{}/{}/{}.jpg".format(folder, code, mvid) for mvid, code in self.mvids_set.items() }
        self.paths = [img_path for mvid, img_path in self.mvid_img_paths.items()]
        img_paths_not_exist = { mvid: img_path for mvid, img_path in self.mvid_img_paths.items() if not os.path.isfile(img_path) }
        if len(img_paths_not_exist) > 0:
            print("\t{} images do not exist... DOWNLOADING them!".format(len(img_paths_not_exist)))
            download_items = [(MtgImages.URL.format(mvid) , img_path) for mvid, img_path in img_paths_not_exist.items()]
            proxyDownloader.downloadThreadedProxies(download_items)
            print("\tFinished downloading {} images!".format(len(img_paths_not_exist)))

    def getImage(self, mvid):
        if mvid in self.mvid_img_paths:
            return cv.imread(self.mvid_img_paths[mvid])

    def getImages(self, mvids):
        return [self.getImage(mvid) for mvid in mvids]