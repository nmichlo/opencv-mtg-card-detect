from mtg.cardDetect import CardDetector
from mtg.data.annFeatures import AnnFeatures
from mtg.data.mtgImages import MtgImages
import cv2 as cv

# mtgImages = MtgImages(["battle for zendikar", "journey into nyx", "born of the gods", "theros", "Gatecrash"], folder="/Users/Nathan/Home/Local Workspace/mtg/cards")
mtgImages = MtgImages(["journey into nyx"], folder="/Users/Nathan/Home/Local Workspace/mtg/cards")
features = AnnFeatures(mtg_images=mtgImages, name="norm_jou_2", normalise=True, remove_flat=True, features_thresh=1, blur=True, nfeatures=100 ,nrandom=10, bins=256, force_means=False)

capture = cv.VideoCapture(0)
detector = CardDetector(4)

count = 0

for mvid in mtgImages.mvids:
        card = mtgImages.getImage(mvid)

        cv.imshow("cards", card)

        if (cv.waitKey(1000) & 0xFF == ord('q')):
            break

        mvids = features.nearestMvids([card])[0]

        if mvid not in mvids:
            count += 1

print("Total: {} Correct: {} Wrong: {}".format(len(mtgImages.mvids), len(mtgImages.mvids)-count, count))


while(True):
    img = capture.read()[1]

    card_bounds = detector.detect(img)
    card_images = detector.extract(img, card_bounds)

    if len(card_images) > 0:
        mvids = features.nearestMvids(card_images, nnearest=3)[0]
        names = [mtgImages.mtg_sets.mvidToCardName(mvid) for mvid in mvids]
        print("names: {}".format(names))
        cv.imshow("seeing", card_images[0])
        detector.showCards(mtgImages.getImages(mvids))

    if (cv.waitKey(1) & 0xFF == ord('q')):
        break